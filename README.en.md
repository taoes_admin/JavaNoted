[中文](./README.md) | **English**

---

This is Personal learning notes and it mainly their own recent learning content records and interview knowledge points , otherwise some latest views and questions will be recorded. 

The document are published in [✨ My Personal Blog Taoes's WebSite](https://www.zhoutao123.com) and [YuqueDoc](https://www.yuque.com/zhoutao123). If you are interested in my document project, please star and support, if it helps you ,you can submit PR 🔀  Your support will support me that I container write more document about java or others!

+ [Java Vitual Machine](https://www.zhoutao123.com/page/book/1)
+ [Design Of Backend Architecture](https://www.zhoutao123.com/page/book/2)
+ [Code Advance Of Java](https://www.zhoutao123.com/page/book/3)
+ [Nginx Notes](https://www.zhoutao123.com/page/book/4)
+ [Frontend Noted](https://www.zhoutao123.com/page/book/5)
+ [Design Pattern Notes](https://www.zhoutao123.com/page/book/6)
+ [Devops Best Practice Guide](https://www.zhoutao123.com/page/book/7)
+ [netty getting started and fighting](https://www.zhoutao123.com/page/book/8)  
+ [High Performance Of MySQL](https://www.zhoutao123.com/page/book/9)
+ [JavaEE Common Framework](https://www.zhoutao123.com/page/book/10)
+ [Java Concurrent](https://www.zhoutao123.com/page/book/11)
+ [Distributed Systems](https://www.zhoutao123.com/page/book/12)
+ [Data Structure And Algorithm](https://www.zhoutao123.com/page/book/13)

The project currently covers Java, JVM, Java concurrency, Devops, design patterns, architecture design, introduction to various middleware and principles, etc. due to limited personal capabilities, if there is any document error, please note that thank you very much

---
##Links
+  🎉 [MyBlog](https://www.zhoutao123.com)
+  🎉 [Gitee](https://gitee.com/taoes_admin/JavaNoted)
+  🎉 [Github]( https://github.com/taoes/JavaNoted)

##  📖  Catalog

###  🔥  JavaWeb  Framework
+ [Spring source analysis](./java/spring/README.md)
+ [Springboot annotation source analysis](./java/spring_boot/README.md)
+ [Spring cloud components and principles](./java/spring_cloud/README.md)
+ [Introduction and principle of mybatis](./java/mybatis/README.md)

###  🔥  JavaSE
+ [Advanced Java programming](./java/java-se)
+ [Java virtual machine](./java/jvm/README.md)
+ [Java Concurrent Programming](./java/concurrent/README.md)
  
###  ✨ Other
+ [Distributed system](./java/distributed/README.md)

## 🧑🏻‍ 💼  Interview
+ [Summary of knowledge points for Java interview](./interview)